#!/bin/bash

date=$(date +"%Y-%m-%d-%H")
data=/tmp/${date}

mkdir $data
tar -cvzf $data.tar.gz /tmp/$date
cp $data.tar.gz ~/ifpb_lista1-atividade
rm -rf $data*
echo "Diretorio criado!"
