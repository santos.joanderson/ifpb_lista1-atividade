#!/bin/bash

echo "Frases Motivacionais:"
sleep 2


echo -e "\e[1;33m As vezes parar é a melhor forma de impulsionar para frente!  \e[0m"
sleep 2

echo -e "\e[1;31m Esforce-se para não ser um sucesso, mas sim para ser valioso!  \e[0m"
sleep 2
