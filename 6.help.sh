#!/bin/bash

echo "1 - Subtituição de variaveis: é a tecnica de definir  e referenciar valores dinamicamente em um script ou arquivo"

echo "Exemplo: 
	nome ="João"
	echo João
"
echo ""
sleep 2


echo "2 - Subtituição de shell: é a tecnica de substituir o resultado de um comando dentro de uma expressão usando o simbolo '\$()'"

echo "Exemplo: echo 'Eu estou no diretorio '\$(pwd)'"
sleep 2

echo ""

echo "3 - Substituição aritmetica: é a tecnica de realizar operações aritméticas dentro de uma expressão"
echo "Exemplo: x=10, echo '\$((x+3))' saida: 13"

